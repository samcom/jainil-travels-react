import Navbar from "./Navbar";
import Footer from "./Footer";
import HeadBanner from "./HeadBanner";

export { Navbar, Footer, HeadBanner };

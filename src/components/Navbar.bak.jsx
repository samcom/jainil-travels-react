import React, { useState } from "react";
import Logo from "../assets/logo.webp";
import {
  FaBars,
  FaEnvelope,
  FaFacebook,
  FaLinkedinIn,
  FaPhone,
  FaTimes,
  FaTwitter,
  FaYoutube,
  FaChevronCircleUp,
} from "react-icons/fa";
import { Link } from "react-router-dom";
import Whatsapp from "../assets/whatsapp.svg";

const Navbar = () => {
  const [nav, setNav] = useState(false);
  const [top, setTop] = useState(false);

  const handleTop = () => {
    window.scrollTo({
      top: 0,
      behavior: "smooth",
    });
  };

  const hideScroll = () => {
    if (window.scrollY >= 80) {
      setTop(true);
    } else {
      setTop(false);
    }
  };

  window.addEventListener("scroll", hideScroll);

  return (
    <>
      <div className="w-full flex gradient flex-col justify-center items-center lg:pt-[30px] text-white z-50">
        <div className="hidden lg:flex justify-between xl:w-3/5 w-11/12 border-b border-[#ffffff33] pb-5">
          <div className="">
            <img src={Logo} alt="" className="w-[170px]" />
          </div>
          <div className="flex gap-7 items-center">
            <a
              className="grid grid-flow-col justify-start text-sm font-bold gap-3"
              href="tel:+919510429400"
              target="_blank"
              rel="noreferrer"
            >
              <FaPhone className="mt-1" /> +91 9510429400
            </a>
            <a
              className="grid grid-flow-col justify-start text-sm font-bold gap-3"
              href="mailto:info@jainiltravels.com"
              target="_blank"
              rel="noreferrer"
            >
              <FaEnvelope className="mt-1" /> info@jainiltravels.com
            </a>
            <FaFacebook />
            <FaLinkedinIn />
            <FaTwitter />
            <a
              href="https://www.youtube.com/channel/UCR2ygXErvJ7KTJmyvX1ZORQ/featured"
              target="_blank"
              rel="noreferrer"
            >
              <FaYoutube />
            </a>
          </div>
        </div>
        <img src={Logo} alt="" className="w-24 lg:hidden" />
        <div className="sticky z-50 top-0 flex lg:hidden justify-between w-11/12 items-center p-5">
          {nav ? (
            <FaTimes
              className="text-2xl"
              onClick={() => {
                setNav(!nav);
              }}
            />
          ) : (
            <FaBars
              className="text-2xl"
              onClick={() => {
                setNav(!nav);
              }}
            />
          )}
        </div>
        <ul
          className={`${
            nav ? "flex bg-black w-full absolute top-20" : "hidden"
          } lg:flex lg:flex-row flex-col gap-10 items-center py-[20px] font-bold`}
        >
          <Link to="/">
            <li className="">Home</li>
          </Link>
          <Link to="/introduction">
            <li className="">Introduction</li>
          </Link>
          <Link to="/about">
            <li className="">About Us</li>
          </Link>
          <Link to="/one-way">
            <li className="">One Way Cab</li>
          </Link>
          <Link to="/round-trip">
            <li className="">Round Trip Cab</li>
          </Link>
          <Link to="/inquiry">
            <li className="">Inquiry</li>
          </Link>
          <Link to="/contact">
            <li className="">Contact Us</li>
          </Link>
        </ul>
      </div>

      <div
        className={`fixed bottom-5 right-5 flex items-center justify-center ${
          !top ? "hidden" : "block"
        }`}
      >
        <div className="h-14 w-14 bg-accent absolute rounded-full animate-ping opacity-20 -z-50"></div>
        <p
          className="navbar rounded-full h-14 w-14 text-white inline-flex gradient"
          onClick={() => handleTop()}
        >
          <FaChevronCircleUp className="h-14 w-14 navbar rounded-full" />
        </p>
      </div>

      <div className="fixed bottom-5 left-5 z-50">
        <a
          href="https://wa.me/919510429400?text=Hello%20Jainil%20Travels%2C%20I%20want%20a%20texi%20service."
          target="_blank"
          rel="noreferrer"
        >
          <img src={Whatsapp} alt="" className="h-12 w-12 z-50" />
        </a>
      </div>
    </>
  );
};

export default Navbar;

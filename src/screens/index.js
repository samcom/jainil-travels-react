import Home from "./Home";
import Introduction from "./Introduction";
import Contact from "./Contact";
import About from "./About";
import RoundTrip from "./RoundTrip";
import OneWay from "./OneWay";

import NotFound from "./NotFound";

export { Home, NotFound, Introduction, Contact, About, RoundTrip, OneWay };
